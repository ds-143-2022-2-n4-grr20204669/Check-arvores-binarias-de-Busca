#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
    int info;
    struct arvore *esquerda;
    struct arvore *direita;
} Arvore;

int buscar (Arvore *a, int v) {
    // se nao encontrar
    if (a == NULL) { return 0; }
    else if (v < a->info) {
        return buscar (a->esquerda, v);
    }
    else if (v > a->info) {
        return buscar (a->direita, v);
    }
    // se encontrar
    else { return 1; }
}

Arvore* inserir (Arvore *a, int v) {
    if (a == NULL) {
        a = (Arvore*)malloc(sizeof(Arvore));
        a->info = v;
        a->esquerda = a->direita = NULL;
    }
    else if (v < a->info) {
        a->esquerda = inserir (a->esquerda, v);
    }
    else { a->direita = inserir (a->direita, v); }
    return a;
}

void in_order(Arvore *a){
    if(!a)
        return;
    in_order(a->esquerda);
    printf("%d ",a->info);
    in_order(a->direita);
}

Arvore* remover(Arvore *a, int x){
    Arvore * aux, * pai_aux;
    int filhos = 0,tmp;

    if(!a)
        return(NULL);

    if(a->info < x)
        a->direita = remover(a->direita, x);
    else if(a->info > x)
        a->esquerda = remover(a->esquerda,x);
    else{
        if(a->esquerda)
            filhos++;
        if(a->direita)
            filhos++;

        if(filhos == 0){
            free(a);
            return(NULL);
        }
        else if(filhos == 1){
            aux = a->esquerda ? a->esquerda : a->direita;
            free(a);
            return(aux);
        }
        else{
            aux = a->esquerda;
            pai_aux = a;
            while(aux->direita){ pai_aux = aux; aux = aux->direita; }
            tmp = a->info;
            a->info = aux->info;
            aux->info = tmp;
            pai_aux->direita = remover(aux,tmp);
            return(a);
        }
    }
    return(a);
}

void print(Arvore * a,int spaces){
    int i;
    for(i= 0; i< spaces; i++) {
        printf(" ");
    }

    if(!a){
        printf("//\n");
        return;
    }

    printf("%d\n", a->info);
    print(a->esquerda, spaces+2);
    print(a->direita, spaces+2);
}

int minValue(Arvore *a){
    Arvore *aux = a;
    while(aux->esquerda != NULL)
        aux = aux->esquerda;

    return aux->info;
}

int maxValue(Arvore *a){
    Arvore *aux = a;
    while(aux->direita != NULL)
        aux = aux->direita;

    return aux->info;
}

int check_arvore_binaria(Arvore *a){
    if(a == NULL) return 1;

    // verifica se valor da esquerda � > que a raiz verificada
    if(a->esquerda != NULL && maxValue(a->esquerda) > a->info)
        return 0;
    // verifica se valor da direita � < que a raiz verificada
    if(a->direita != NULL && minValue(a->direita)<= a->info)
        return 0;
    // a �rvore continua percorrida
    if(!check_arvore_binaria(a->esquerda) || !check_arvore_binaria(a->direita))
        return 0;

    return 1;
}

int main(){
    Arvore * a;

    a = inserir(NULL, 50);
    a = inserir(a, 30);
    a = inserir(a, 90);
    a = inserir(a, 20);
    a = inserir(a, 40);
    a = inserir(a, 95);
    a = inserir(a, 10);
    a = inserir(a, 35);
    a = inserir(a, 45);

    printf("\n");

    printf("Arvore Binaria: %d\n", check_arvore_binaria(a));

}
